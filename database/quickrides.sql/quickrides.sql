-- phpMyAdmin SQL Dump
-- version 3.3.9
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 29, 2017 at 06:38 AM
-- Server version: 5.1.53
-- PHP Version: 5.3.4

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `quickrides`
--

-- --------------------------------------------------------

--
-- Table structure for table `tblbooking`
--

CREATE TABLE IF NOT EXISTS `tblbooking` (
  `id` int(11) NOT NULL,
  `userEmail` varchar(100) DEFAULT NULL,
  `VehicleId` int(11) DEFAULT NULL,
  `FromDate` varchar(20) DEFAULT NULL,
  `ToDate` varchar(20) DEFAULT NULL,
  `message` varchar(255) DEFAULT NULL,
  `Status` int(11) DEFAULT NULL,
  `PostingDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblbooking`
--

INSERT INTO `tblbooking` (`id`, `userEmail`, `VehicleId`, `FromDate`, `ToDate`, `message`, `Status`, `PostingDate`) VALUES
(0, 'info.antonykiragu@gmail.com', 0, '2017-09-30', '2017-10-20', '\r\nSTRIKING DESIGN FEATURES.\r\n\r\nThe new BMW X3’s exterior and interior have received a complete redesign, with particular attention paid to enhancing its sophistication and versatility. Brand new features such as the Phytonic Blue exterior paintwork – curr', 1, '2017-09-29 09:12:32'),
(0, 'info.antonykiragu@gmail.com', 0, '2017-09-22', '2017-09-30', '\r\nSTRIKING DESIGN FEATURES.\r\n\r\nThe new BMW X3’s exterior and interior have received a complete redesign, with particular attention paid to enhancing its sophistication and versatility. Brand new features such as the Phytonic Blue exterior paintwork – curr', 0, '2017-09-29 09:13:42');

-- --------------------------------------------------------

--
-- Table structure for table `tblbrands`
--

CREATE TABLE IF NOT EXISTS `tblbrands` (
  `id` int(11) NOT NULL,
  `BrandName` varchar(120) NOT NULL,
  `CreationDate` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblbrands`
--

INSERT INTO `tblbrands` (`id`, `BrandName`, `CreationDate`) VALUES
(1, 'Maruti', '2017-09-28 16:24:34'),
(2, 'BMW', '2017-09-28 16:24:50'),
(3, 'Audi', '2017-09-28 16:25:03'),
(4, 'Nissan', '2017-09-28 16:25:13'),
(5, 'Toyota', '2017-09-28 16:25:24'),
(7, 'Marutiu', '2017-09-28 06:22:13');

-- --------------------------------------------------------

--
-- Table structure for table `tblcontactusinfo`
--

CREATE TABLE IF NOT EXISTS `tblcontactusinfo` (
  `id` int(11) NOT NULL,
  `Address` tinytext,
  `EmailId` varchar(255) DEFAULT NULL,
  `ContactNo` char(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblcontactusinfo`
--


-- --------------------------------------------------------

--
-- Table structure for table `tblcontactusquery`
--

CREATE TABLE IF NOT EXISTS `tblcontactusquery` (
  `id` int(11) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `EmailId` varchar(120) DEFAULT NULL,
  `ContactNumber` char(11) DEFAULT NULL,
  `Message` longtext,
  `PostingDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblcontactusquery`
--


-- --------------------------------------------------------

--
-- Table structure for table `tblusers`
--

CREATE TABLE IF NOT EXISTS `tblusers` (
  `id` int(11) NOT NULL,
  `FullName` varchar(120) DEFAULT NULL,
  `EmailId` varchar(100) DEFAULT NULL,
  `Password` varchar(100) DEFAULT NULL,
  `ContactNo` char(11) DEFAULT NULL,
  `dob` varchar(100) DEFAULT NULL,
  `Address` varchar(255) DEFAULT NULL,
  `City` varchar(100) DEFAULT NULL,
  `Country` varchar(100) DEFAULT NULL,
  `RegDate` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblusers`
--

INSERT INTO `tblusers` (`id`, `FullName`, `EmailId`, `Password`, `ContactNo`, `dob`, `Address`, `City`, `Country`, `RegDate`) VALUES
(0, 'Antony', 'info.antonykiragu@gmail.com', '9cd7f69e5d2b025ab1438471e0711e79', '0713236701', NULL, NULL, NULL, NULL, '2017-09-29 08:56:13');

-- --------------------------------------------------------

--
-- Table structure for table `tblvehicles`
--

CREATE TABLE IF NOT EXISTS `tblvehicles` (
  `id` int(11) NOT NULL,
  `VehiclesTitle` varchar(150) DEFAULT NULL,
  `VehiclesOrigin` varchar(255) NOT NULL,
  `VehiclesBrand` int(11) DEFAULT NULL,
  `VehiclesOverview` longtext,
  `PricePerDay` int(11) DEFAULT NULL,
  `FuelType` varchar(100) DEFAULT NULL,
  `ModelYear` int(6) DEFAULT NULL,
  `SeatingCapacity` int(11) DEFAULT NULL,
  `Vimage1` varchar(120) DEFAULT NULL,
  `Vimage2` varchar(120) DEFAULT NULL,
  `Vimage3` varchar(120) DEFAULT NULL,
  `Vimage4` varchar(120) DEFAULT NULL,
  `Vimage5` varchar(120) DEFAULT NULL,
  `AirConditioner` int(11) DEFAULT NULL,
  `PowerDoorLocks` int(11) DEFAULT NULL,
  `AntiLockBrakingSystem` int(11) DEFAULT NULL,
  `BrakeAssist` int(11) DEFAULT NULL,
  `PowerSteering` int(11) DEFAULT NULL,
  `DriverAirbag` int(11) DEFAULT NULL,
  `PassengerAirbag` int(11) DEFAULT NULL,
  `PowerWindows` int(11) DEFAULT NULL,
  `CDPlayer` int(11) DEFAULT NULL,
  `CentralLocking` int(11) DEFAULT NULL,
  `CrashSensor` int(11) DEFAULT NULL,
  `LeatherSeats` int(11) DEFAULT NULL,
  `RegDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblvehicles`
--

INSERT INTO `tblvehicles` (`id`, `VehiclesTitle`, `VehiclesOrigin`, `VehiclesBrand`, `VehiclesOverview`, `PricePerDay`, `FuelType`, `ModelYear`, `SeatingCapacity`, `Vimage1`, `Vimage2`, `Vimage3`, `Vimage4`, `Vimage5`, `AirConditioner`, `PowerDoorLocks`, `AntiLockBrakingSystem`, `BrakeAssist`, `PowerSteering`, `DriverAirbag`, `PassengerAirbag`, `PowerWindows`, `CDPlayer`, `CentralLocking`, `CrashSensor`, `LeatherSeats`, `RegDate`) VALUES
(0, 'The new BMW X3', '', 2, 'Featuring brand new engines, enhanced design features, innovative technologies and a generous specification, the new BMW X3 is an advocate for unlimited opportunity. And thanks to semi-autonomous driving systems and the intelligent all-wheel drive system BMW xDrive, it promises an exceptional ride experience that’s unrivalled in the SUV class', 2500, 'Petrol', 2016, 6, '047.jpg', 'BMW-730-Ld-White-1.jpg', 'bmw-x6-front-front-new.jpg', 'da7085b5b9b63cdbed109fc4cfff074e.jpg', 'images.jpg', 1, NULL, NULL, NULL, NULL, 1, 1, NULL, 1, NULL, NULL, NULL, '2017-09-29 08:59:57'),
(0, ' A5 Coupe 2018', 'Germany', 3, 'Designed with a sharp exterior, reimagined interior and 252 horsepower, the Audi A5 Coupe is athletic, elegant and exciting. And with its available driver assistance technologies and manual transmission, it’s sophisticated and sporty as well. ', 3200, 'Petrol', 2017, 6, '2017-audi-s8-quattro-tiptronic-plus-sedan-angular-front.png', 'audi-a3-cabriolet.jpg', 'audi-rs7-cutout_0.jpg', 'pre-2018-audi-s5-cabriolet-profile.jpg', 'r8v10-flyout.png', 1, 1, 1, 1, NULL, 1, 1, 1, 1, 1, 1, 1, '2017-09-29 09:26:56'),
(0, 'marutisuzuki', 'Japan', 1, 'Currently 14 Maruti Suzuki cars are available for sale in India. The lowest price Maruti Suzuki model is the Maruti Omni at Rs 2.37 Lakh and the highest price model is the Maruti Ciaz at Rs 11.55 Lakh. Upcoming Maruti Suzuki car launches include Maruti S-Cross, Maruti Swift, Maruti Wagon R, Maruti Grand Vitara. ', 2000, 'Diesel', 2014, 9, 'maruti-suzuki-ignis-tinsel-blue-pear-arctic-white.png', 'maruti-suzuki-vitara-brezza-blazing-red.jpg', 'sift_600x300.jpg', '047.jpg', 'marutisuzuki_celerio_600x300.jpg', 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2017-09-29 09:32:14'),
(0, 'ALMERA', 'Malaysia', 4, 'If you already know Nissan Almera is for you, you can skip the details and build yours now. Need a little more proof? Just keep scrolling.', 3000, 'Petrol', 2015, 6, '2015-nissan-juke-sl-suv-angular-front.png', 'Overview_main_M_7.png.ximg.m_12_h.smart.png', 'packshot_colorpicker_JUKE_QAB_medium.png.ximg.m_12_h.smart.png', 'Elgrand v1.png.ximg.l_full_m.smart.png', 'images.jpg', NULL, NULL, NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, 1, NULL, '2017-09-29 09:35:56');
